// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package dao

import "pearnote/model"

type UserDao struct {
	BaseDao
}

func (dao *UserDao) FindByPhone(phone string) (*model.User, error) {
	user := &model.User{Phone: phone}
	if has, err := db.Get(user); has {
		return user, err
	} else {
		return nil, err
	}
}
func (dao *UserDao) FindEmail(email string) (*model.User, error) {
	user := &model.User{Email: email}
	if has, err := db.Get(user); has {
		return user, err
	} else {
		return nil, err
	}
}
func (dao *UserDao) FindById(id int64) (*model.User, error) {
	user := &model.User{Id: id}
	if has, err := db.Get(user); has {
		return user, err
	} else {
		return nil, err
	}
}
