// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package dao

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	_ "github.com/mattn/go-sqlite3"
	log "github.com/sirupsen/logrus"
	"os"
	"path/filepath"
	"pearnote/config"
	"pearnote/model"
	"time"
)

var db *xorm.Engine

func Init() {
	conf := config.GetCurrentConfig()
	if "sqlite3" == conf.DB {
		conf.Conn = filepath.Join(config.AppPath, "data", conf.Conn)
	}
	engine, err := xorm.NewEngine(conf.DB, conf.Conn)

	if err != nil {
		log.Fatalf("db connect failed: %#v\n", err.Error())
		os.Exit(-1)
	}
	if engine.Ping() != nil {
		log.Fatalf("db connect error: %#v\n", err.Error())
		os.Exit(-1)
	}
	if err := engine.Sync2(new(model.User), new(model.DocumentNode), new(model.DocumentContent), new(model.Image)); err != nil {
		log.Fatalf("sync error: %#v\n", err.Error())
		os.Exit(-1)
	}

	engine.ShowSQL(false)
	engine.SetMaxIdleConns(5)
	engine.SetMaxOpenConns(30)
	engine.TZLocation, _ = time.LoadLocation("Asia/Shanghai")
	log.Info("[NewDBEngine] connect to db success")

	timer := time.NewTicker(time.Minute * 30)
	go func(engine *xorm.Engine) {
		for _ = range timer.C {
			err = engine.Ping()
			if err != nil {
				log.Fatalf("db connect error: %#v\n", err.Error())
			}
		}
	}(engine)
	db = engine
}

func GetDB() *xorm.Engine {
	return db
}
