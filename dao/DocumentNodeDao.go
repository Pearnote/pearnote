// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package dao

import "pearnote/model"

type DocumentNodeDao struct {
	BaseDao
}

func (dao *DocumentNodeDao) ListByPid(uid int64, category int, pid int64) (*[]model.DocumentNode, error) {
	objs := make([]model.DocumentNode, 0)
	if err := db.Where("category =? and pid=? and user_id=?", category, pid, uid).Find(&objs); err != nil {
		return nil, err
	} else {
		return &objs, nil
	}
}

func (dao *DocumentNodeDao) FindOne(id int64) (*model.DocumentNode, error) {
	bean := &model.DocumentNode{}
	if has, err := db.Id(id).Get(bean); has == false || err != nil {
		return nil, err
	} else {
		return bean, nil
	}
}
