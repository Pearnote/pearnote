// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package dao

var userDao *UserDao
var documentNodeDao *DocumentNodeDao
var documentContentDao *DocumentContentDao
var imageDao *ImageDao

func init() {
	documentNodeDao = &DocumentNodeDao{}
	documentContentDao = &DocumentContentDao{}
	userDao = &UserDao{}
	imageDao = &ImageDao{}
}

func GetUserDao() *UserDao {
	return userDao
}
func GetImageDao() *ImageDao {
	return imageDao
}
func GetDocumentNodeDao() *DocumentNodeDao {
	return documentNodeDao
}
func GetDocumentContentDao() *DocumentContentDao {
	return documentContentDao
}
