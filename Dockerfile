FROM scratch
WORKDIR /home/webapp/
COPY app /home/webapp/
EXPOSE 9090
CMD ["/home/webapp/app"]

