package main

import (
	"github.com/spf13/cobra"
	"pearnote/cmd"
)

func main() {

	cobra.MousetrapHelpText = ""
	cmd.Execute()
}
