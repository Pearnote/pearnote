// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package router

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"pearnote/config"
	"pearnote/dao"
	"pearnote/log"
	"pearnote/middleware"
	"pearnote/model"
	"pearnote/response"
	"pearnote/service"
)

const (
	appVersion = "0.0.1"
	apiV1      = "/xhr/v1"
)

var authMiddleware = middleware.NewJWT()

func init() {
	authMiddleware.Authenticator = func(c *gin.Context) (interface{}, error) {
		userVO := model.UserLoginVO{}
		if err := c.ShouldBind(&userVO); err != nil {
			return nil, err
		}
		user, err := service.GetUserService().Login(&userVO)
		if err != nil {
			return nil, err
		}
		return user, nil
	}

}
func StartServer() {

	config.Init()
	log.Init()
	dao.Init()
	service.Init()

	r := gin.Default()
	r.Use(log.LogToFile())
	//r.Use(middleware.ExceptionHandler())
	InitStaticRouter(r)
	InitUserRouter(r)
	InitDocContentRouter(r)
	InitDocNodeRouter(r)
	InitImageRouter(r)

	r.GET("/health", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, response.Ok(nil))
	})
	r.GET("/version", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, response.Ok(appVersion))
	})

	if err := r.Run(":" + config.GetCurrentConfig().Listen); err != nil {
		fmt.Println(err)
	}
}
