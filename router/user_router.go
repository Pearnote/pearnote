// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package router

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"pearnote/model"
	"pearnote/service"
)

func InitUserRouter(r *gin.Engine) {
	userService := service.GetUserService()
	userRouter := r.Group(apiV1 + "/users")
	{
		userRouter.POST("/register", func(ctx *gin.Context) {
			userVO := model.UserRegVO{}
			if err := ctx.ShouldBind(&userVO); err != nil {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusBadRequest, Msg: err.Error(), Data: nil})
				return
			}
			id, err := userService.Register(&userVO)
			if err != nil {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusBadRequest, Msg: err.Error(), Data: nil})
			} else {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusOK, Msg: "success", Data: id})
			}
		})

		userRouter.POST("/login", authMiddleware.LoginHandler)
	}
}
