// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package router

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"pearnote/model"
	"pearnote/service"
	"pearnote/utils"
	"strconv"
)

const (
	WIKI = 1
)

func InitDocNodeRouter(r *gin.Engine) {
	docNodeService := service.GetDocNodeService()
	docNodeRouter := r.Group(apiV1 + "/docNodes")
	docNodeRouter.Use(authMiddleware.MiddlewareFunc())
	{

		docNodeRouter.GET("/", func(ctx *gin.Context) {
			uid := utils.ExtractUid(ctx)
			category, _ := strconv.Atoi(ctx.Query("category"))
			pid, _ := strconv.ParseInt(ctx.Query("pid"), 10, 64)
			ret, err := docNodeService.List(uid, category, pid)
			if err != nil {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusBadRequest, Msg: err.Error(), Data: nil})
			} else {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusOK, Msg: "success", Data: ret})
			}
		})

		docNodeRouter.PUT("/", func(ctx *gin.Context) {
			uid := utils.ExtractUid(ctx)
			vo := model.DocumentNode{}
			if err := ctx.ShouldBind(&vo); err != nil {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusBadRequest, Msg: err.Error(), Data: nil})
				return
			}
			vo.UserId = uid

			ret, err := docNodeService.Save(&vo)
			if err != nil {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusBadRequest, Msg: err.Error(), Data: nil})
			} else {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusOK, Msg: "success", Data: ret})
			}

		})

		docNodeRouter.DELETE("/:id", func(ctx *gin.Context) {
			uid := utils.ExtractUid(ctx)
			id, _ := strconv.ParseInt(ctx.Param("id"), 10, 64)
			ret, err := docNodeService.Delete(id, uid)
			if err != nil {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusBadRequest, Msg: err.Error(), Data: nil})
			} else {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusOK, Msg: "success", Data: ret})
			}

		})
	}
}
