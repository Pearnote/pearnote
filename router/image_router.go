// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package router

import (
	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	"net/http"
	"path/filepath"
	"pearnote/config"
	"pearnote/model"
	"pearnote/service"
	"strconv"
	"strings"
)

func InitImageRouter(r *gin.Engine) {
	imageService := service.GetImageService()
	imageRouter := r.Group(apiV1 + "/images")
	imageRouter.Use(authMiddleware.MiddlewareFunc())
	{
		imageRouter.POST("/", func(c *gin.Context) {
			pid, _ := strconv.ParseInt(c.Param("pid"), 10, 64)
			file, _ := c.FormFile("file")
			ukId := strings.ReplaceAll(uuid.NewV4().String(), "-", "")[8:24]
			suffix := file.Filename[strings.LastIndex(file.Filename, "."):]
			fileName := ukId + suffix
			savePath := filepath.Join(config.AppPath, "data", "images", fileName)
			if err := c.SaveUploadedFile(file, savePath); err != nil {
				c.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusBadRequest, Msg: err.Error(), Data: nil})
			} else {
				image := &model.Image{
					Pid:      pid,
					FileName: fileName,
				}
				_, err := imageService.Save(image)
				if err != nil {
					c.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusBadRequest, Msg: err.Error(), Data: nil})
				} else {
					ret := "/images/" + fileName
					c.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusOK, Msg: "success", Data: ret})
				}
			}
		})
	}
}
