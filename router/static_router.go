// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package router

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"path/filepath"
	"pearnote/config"
)

func InitStaticRouter(r *gin.Engine) {
	r.StaticFS("/css", http.Dir(filepath.Join(config.AppPath, "public", "css")))
	r.StaticFS("/img", http.Dir(filepath.Join(config.AppPath, "public", "img")))
	r.StaticFS("/images", http.Dir(filepath.Join(config.AppPath, "data", "images")))
	r.StaticFS("/js", http.Dir(filepath.Join(config.AppPath, "public", "js")))
	r.StaticFS("/fonts", http.Dir(filepath.Join(config.AppPath, "public", "fonts")))
	r.StaticFile("/", filepath.Join(config.AppPath, "public", "index.html"))
	r.StaticFile("/favicon.ico", filepath.Join(config.AppPath, "public", "favicon.ico"))
}
