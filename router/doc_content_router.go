// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package router

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"pearnote/model"
	"pearnote/service"
	"pearnote/utils"
	"strconv"
)

func InitDocContentRouter(r *gin.Engine) {
	docContentService := service.GetDocContentService()
	docContentRouter := r.Group(apiV1 + "/docContents")
	docContentRouter.Use(authMiddleware.MiddlewareFunc())
	{
		docContentRouter.GET("/:id", func(ctx *gin.Context) {
			uid := utils.ExtractUid(ctx)
			id, _ := strconv.ParseInt(ctx.Param("id"), 10, 64)
			ret, err := docContentService.GetById(id, uid)
			if err != nil {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusBadRequest, Msg: err.Error(), Data: nil})
			} else {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusOK, Msg: "success", Data: ret})
			}
		})
		docContentRouter.PUT("/", func(ctx *gin.Context) {
			uid := utils.ExtractUid(ctx)
			vo := model.DocumentContent{}
			if err := ctx.ShouldBind(&vo); err != nil {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusBadRequest, Msg: err.Error(), Data: nil})
				return
			}
			ret, err := docContentService.Save(&vo, uid)
			if err != nil {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusBadRequest, Msg: err.Error(), Data: nil})
			} else {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusOK, Msg: "success", Data: ret})
			}
		})

		docContentRouter.DELETE("/:id", func(ctx *gin.Context) {
			uid := utils.ExtractUid(ctx)
			id, _ := strconv.ParseInt(ctx.Param("id"), 10, 64)
			ret, err := docContentService.Delete(id, uid)
			if err != nil {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusBadRequest, Msg: err.Error(), Data: nil})
			} else {
				ctx.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusOK, Msg: "success", Data: ret})
			}
		})
	}
}
