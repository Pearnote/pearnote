// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package model

type DocumentNode struct {
	Id        int64  `json:"id"`
	Pid       int64  `json:"pid"`
	UserId    int64  `json:"userId"`
	Category  int    `json:"category"`
	Name      string `xorm:"varchar(64) notnull 'name'" json:"name"`
	Remark    string `xorm:"varchar(128) 'remark'" json:"remark"`
	State     int    `json:"state"`
	CreatedAt int64  `xorm:"created" json:"createdAt"`
	UpdatedAt int64  `xorm:"updated" json:"updatedAt"`
}

type DocumentContent struct {
	Id        int64  `json:"id"`
	Content   string `json:"content"`
	CreatedAt int64  `xorm:"created",json:"createdAt"`
	UpdatedAt int64  `xorm:"updated",json:"updatedAt"`
}

type DocumentNodeVO struct {
	DocumentNode
	Children *[]DocumentNodeVO `json:"children"`
}
