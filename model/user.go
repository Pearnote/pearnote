// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package model

type User struct {
	Id         int64  `json:"id"`
	Email      string `json:"email"`
	Phone      string `json:"phone"`
	Salt       string `json:"salt"`
	Password   string `json:"password"`
	FolderRoot int64  `json:"folderRoot"`
	CreatedAt  int64  `xorm:"created",json:"createdAt"`
	UpdatedAt  int64  `xorm:"updated",json:"updatedAt"`
}

type UserRegVO struct {
	Type     int    `json:"type" binding:"required"`
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type UserLoginVO struct {
	Type     int    `json:"type"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type UserVO struct {
	Id         int64 `json:"id"`
	FolderRoot int64 `json:"folderRoot"`
}
