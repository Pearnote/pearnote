// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package utils

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
	"math"
	"math/rand"
	"os"
	"regexp"
	"time"
)

func GenRandomCode(len int) string {
	return fmt.Sprintf(fmt.Sprintf("%s%d%s", "%0", len, "v"), rand.New(rand.NewSource(time.Now().UnixNano())).Int31n(int32(math.Pow10(len))))
}
func Md5WithCount(s string, n int) string {
	ret := ""
	for i := 0; i < n; i++ {
		ret = Md5(s)
	}
	return ret
}
func Md5(s string) string {
	h := md5.New()
	h.Write([]byte(s))
	return hex.EncodeToString(h.Sum(nil))
}

func Md5Byte(data []byte) string {
	h := md5.New()
	h.Write(data)
	return hex.EncodeToString(h.Sum(nil))
}

func ExtractUid(c *gin.Context) int64 {
	claims := jwt.ExtractClaims(c)
	return int64(claims["id"].(float64))
}
func ExtractUsername(c *gin.Context) string {
	claims := jwt.ExtractClaims(c)
	return claims["name"].(string)
}

func IsPhone(phone string) bool {
	phonePattern := regexp.MustCompile("^[1][3|4|5|6|7|8][0-9]{9}$")
	return phonePattern.MatchString(phone)
}
func IsFileExist(fileName string) bool {
	_, err := os.Stat(fileName) //os.Stat获取文件信息
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		return false
	}
	return true
}

func CreateWhenNotExist(folder string) {
	if !IsFileExist(folder) {
		err := os.MkdirAll(folder, os.ModePerm)
		if err != nil {
			fmt.Printf("mkdir failed![%v]\n", err)
		} else {
			fmt.Printf("mkdir success!\n")
		}
	}
}
