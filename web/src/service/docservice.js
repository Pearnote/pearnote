import axios from "../http/axios";

export const getDocNodes = (params) => {
    return axios.request({
        url: '/docNodes/',
        params: params,
        method: 'get'
    })
};

export const putDocNodes = (data) => {
    return axios.request({
        url: '/docNodes/',
        data: data,
        method: 'put'
    })
};
export const deleteDocNodes = (params) => {
    return axios.request({
        url: '/docNodes/' + params,
        method: 'delete'
    })
};

export const getDocContent = (params) => {
    return axios.request({
        url: '/docContents/' + params,
        method: 'get'
    })
};

export const putDocContent = (data) => {
    return axios.request({
        url: '/docContents/',
        data: data,
        method: 'put'
    })
};
export const deleteDocContent = (params) => {
    return axios.request({
        url: '/docContents/' + params,
        method: 'delete'
    })
};