import axios from '../http/axios'


export const login = (data) => {
    return axios.request({
        url: '/users/login',
        data,
        method: 'post'
    })

};

export const reg = (data) => {
    return axios.request({
        url: '/users/register',
        data,
        method: 'post'
    })

};

