import axios from '../http/axios'


export const saveImage = (data) => {
    return axios.request({
        url: '/images/',
        data: data,
        headers: {'Content-Type': 'multipart/form-data'},
        method: 'post'
    })

};

