const baseUrl = "/xhr/v1";
import axios from 'axios'

class HttpRequest {
    constructor(baseUrl = baseURL) {
        this.baseUrl = baseUrl
        this.queue = {}
    }

    getInsideConfig() {
        return {
            baseURL: this.baseUrl,
            headers: {
                //
            }
        }
    }

    destroy(url) {
        delete this.queue[url]
        if (!Object.keys(this.queue).length) {
            // Spin.hide()
        }
    }

    interceptors(instance, url) {
        // 请求拦截
        instance.interceptors.request.use(config => {
            if (localStorage.getItem('tokenItem')) {  // 判断是否存在token，如果存在的话，则每个http header都加上token
                let tokenItem = JSON.parse(localStorage.getItem('tokenItem'))
                config.headers.Authorization = 'Bearer ' + tokenItem.token;
            }
            this.queue[url] = true;
            return config
        }, error => {
            return Promise.reject(error)
        });
        // 响应拦截
        instance.interceptors.response.use(res => {
            this.destroy(url);
            const {data, status} = res;
            if (status !== 200) {
                return {};
            }
            if (data.code !== 200) {
                if (data.msg) {
                }
                return null;
            }
            return data.data

        }, error => {
            this.destroy(url);
            return Promise.reject(error)
        })
    }

    request(options) {
        const instance = axios.create()
        options = Object.assign(this.getInsideConfig(), options)
        this.interceptors(instance, options.url)
        return instance(options)
    }
}


export default new HttpRequest(baseUrl);
