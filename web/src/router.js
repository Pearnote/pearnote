import Vue from 'vue'
import Router from 'vue-router'
import Login from "./views/Login";
import Home from './views/Home.vue'
import Book from "./views/Book";
import BookContent from "./views/BookContent";
import Reg from "./views/Reg";
import TODOList from "./views/TODOList";

Vue.use(Router);

const router = new Router({
    routes: [{
        path: '/',
        redirect: '/book'
    },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/reg',
            name: 'register',
            component: Reg,
        },
        {
            path: '/home',
            name: 'home',
            component: Home,
            children: [{
                path: '/book',
                name: 'book',
                component: Book
            }, {
                path: '/todo',
                name: 'todo',
                component: TODOList
            }]
        },
        {
            path: '/book-content',
            name: 'book-content',
            component: BookContent,
        }

    ]
});
router.beforeEach((to, from, next) => {
    if (to.name === 'login') {
        localStorage.clear();
        next();
        return
    }
    let token = localStorage.getItem('tokenItem');
    if (!token && to.name !== 'login' && to.name !== 'register') {
        next({name: 'login'});
        return
    }
    next()
});

export default router
