// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package service

import (
	"pearnote/model"
)

type DocumentContentService struct {
}

func (service *DocumentContentService) Save(docContent *model.DocumentContent, uid int64) (int64, error) {
	if currentObj, err := docContentDao.GetById(docContent.Id); err != nil {
		return -1, nil

	} else if currentObj != nil {
		return docContentDao.Update(docContent.Id, docContent)
	} else {
		return docContentDao.Insert(docContent)
	}
}

func (service *DocumentContentService) Delete(id int64, userId int64) (int64, error) {
	return docContentDao.Delete(id, &model.DocumentContent{})
}

func (service *DocumentContentService) GetById(id int64, userId int64) (*model.DocumentContent, error) {
	return docContentDao.GetById(id)
}
