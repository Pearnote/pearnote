// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package service

import (
	"errors"
	"pearnote/model"
)

type DocumentNodeService struct {
}

func docNodeCheck(id int64, uid int64) (bool, error) {
	if id == 0 {
		return true, nil
	}
	if folder, err := docNodeDao.FindOne(id); err != nil {
		return false, errors.New("校验失败")
	} else if folder.UserId != uid {
		return false, errors.New("参数非法")
	} else {
		return true, nil
	}
}
func (service *DocumentNodeService) Save(docNode *model.DocumentNode) (int64, error) {
	if docNode.Id > 0 {
		if isValid, err := docNodeCheck(docNode.Id, docNode.UserId); isValid {
			return docNodeDao.Update(docNode.Id, docNode)
		} else {
			return -1, err
		}
	} else {
		if isValid, err := docNodeCheck(docNode.Pid, docNode.UserId); isValid {
			return docNodeDao.Insert(docNode)
		} else {
			return -1, err
		}
	}
}

func (service *DocumentNodeService) Delete(id int64, userId int64) (int64, error) {
	if isValid, err := docNodeCheck(id, userId); isValid {
		return docNodeDao.Delete(id, &model.DocumentNode{})
	} else {
		return -1, err
	}
}

func (service *DocumentNodeService) List(userId int64, category int, pid int64) (*[]model.DocumentNodeVO, error) {
	if docNodes, err := docNodeDao.ListByPid(userId, category, pid); err != nil {
		return nil, err
	} else {
		objs := make([]model.DocumentNodeVO, 0)
		for _, docNode := range *docNodes {
			docNodeVO := model.DocumentNodeVO{}
			docNodeVO.Id = docNode.Id
			docNodeVO.Pid = docNode.Pid
			docNodeVO.Category = docNode.Category
			docNodeVO.Name = docNode.Name
			docNodeVO.Remark = docNode.Remark
			if docNodeVO.Children, err = service.List(userId, category, docNode.Id); err != nil {
				return nil, err
			} else {
				objs = append(objs, docNodeVO)
			}
		}

		return &objs, nil
	}
}
