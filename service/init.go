// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package service

import (
	"pearnote/dao"
)

var userDao *dao.UserDao
var docNodeDao *dao.DocumentNodeDao
var docContentDao *dao.DocumentContentDao
var imageDao *dao.ImageDao
var docNodeService *DocumentNodeService
var docContentService *DocumentContentService
var userService *UserService
var imageService *ImageService

func Init() {
	userDao = dao.GetUserDao()
	docNodeDao = dao.GetDocumentNodeDao()
	docContentDao = dao.GetDocumentContentDao()
	imageDao = dao.GetImageDao()
	docNodeService = &DocumentNodeService{}
	docContentService = &DocumentContentService{}
	userService = &UserService{}
	imageService = &ImageService{}
}

func GetDocNodeService() *DocumentNodeService {
	return docNodeService
}
func GetDocContentService() *DocumentContentService {
	return docContentService
}
func GetUserService() *UserService {
	return userService
}
func GetImageService() *ImageService {
	return imageService
}
