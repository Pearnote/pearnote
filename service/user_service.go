// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package service

import (
	"github.com/pkg/errors"
	"pearnote/model"
	"pearnote/utils"
)

const (
	PHONE = 1
	EMAIL = 2
)

type UserService struct {
}

func (service *UserService) Register(userRegVO *model.UserRegVO) (int64, error) {
	user := &model.User{}
	if err := utils.BeanCopy(*userRegVO, user); err != nil {
		return -1, err
	} else {
		switch userRegVO.Type {
		case PHONE:
			user.Phone = userRegVO.Username
			break
		case EMAIL:
			user.Email = userRegVO.Username
			break
		}
		user.Salt = utils.GenRandomCode(6)
		user.Password = utils.Md5WithCount(user.Salt+user.Password+user.Salt, 3)
		return userDao.Insert(user)
	}
}

func checkLogin(user *model.User, password string) (*model.UserVO, error) {
	if user.Password == utils.Md5WithCount(user.Salt+password+user.Salt, 3) {
		return &model.UserVO{Id: user.Id, FolderRoot: user.FolderRoot}, nil
	} else {
		return nil, errors.New("用户名或密码错误")
	}
}

func (service *UserService) Login(vo *model.UserLoginVO) (*model.UserVO, error) {
	switch vo.Type {
	case PHONE:
		if user, err := userDao.FindByPhone(vo.Username); err != nil {
			return nil, errors.New("用户名或密码错误")
		} else {
			return checkLogin(user, vo.Password)
		}
	case EMAIL:
		if user, err := userDao.FindEmail(vo.Username); err != nil {
			return nil, errors.New("用户名或密码错误")
		} else {
			return checkLogin(user, vo.Password)
		}
	default:
		return nil, errors.New("参数非法")
	}
}

func (service *UserService) FindByUid(uid int64) (*model.User, error) {
	return userDao.FindById(uid)
}
