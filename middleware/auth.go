// Copyright 2019 Pearnote Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package middleware

import (
	"github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
	"net/http"
	"pearnote/model"
	"time"
)

func NewJWT() *jwt.GinJWTMiddleware {

	authMiddleware := &jwt.GinJWTMiddleware{
		Realm:            "PearNote Zone",
		SigningAlgorithm: "HS256",
		Key:              []byte(")87hkl%%^^knknklpearnote,././,.,"),
		Timeout:          7 * 24 * time.Hour,
		MaxRefresh:       24 * time.Hour,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*model.UserVO); ok {
				return jwt.MapClaims{
					"id": v.Id,
				}
			}
			return jwt.MapClaims{}
		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			return true
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return int64(claims["id"].(float64))
		},
		Unauthorized: func(context *gin.Context, code int, message string) {
			context.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusForbidden, Msg: message})
		},
		LoginResponse: func(context *gin.Context, i int, token string, expire time.Time) {
			ret := map[string]interface{}{
				"token":  token,
				"expire": expire.Unix(),
			}

			context.JSON(http.StatusOK, model.AjaxResult{Code: http.StatusOK, Msg: "success", Data: ret})
		},

		TokenLookup: "header: Authorization, query: token, cookie: jwt",

		TokenHeadName: "Bearer",

		TimeFunc: time.Now,
	}
	return authMiddleware
}
